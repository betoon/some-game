package com.sda.dp.game;


import com.sda.dp.game.view.Window;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Window w = new Window();
//                w.addObjects();
                w.setVisible(true);
            }
        });
    }
}
