package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

public class Heart extends AbstractGameObject {

    private BufferedImage image;

    public Heart() {
        try {
            image = ImageIO.read(new File("src/main/resources/heart_small.png"));
        } catch (IOException e) {
            System.err.println("nimo obrozka synek");
        }
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {

    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, position.x, position.y, null);
    }

    public void paint(Graphics2D g2d, int i) {
        position.x=i*getWidth()-i*25;
        position.y=0;

        paint(g2d);
    }

}
