package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;
import com.sda.dp.game.VerticalDirection;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Monster extends AbstractGameObject {

    private int hp; //wytrzymalosc na hity
    private BufferedImage image;

    private Point initialPosition;
    private int moveRange;

    public Monster(int x, int y, int durability, int range) {
        super(new Point(x, y));
        this.initialPosition = new Point(x, y);
        this.hp = durability;
        this.moveRange = range;
        try {
            image = ImageIO.read(new File("src/main/resources/pig_small.png"));
        } catch (IOException ex) {
            System.err.println("nie moge zaladowac obrazka monstra");
        }

        speed = 4;

        vDir = VerticalDirection.UP;
    }

    @Override
    public void move(double move) {
        //roznica pozycji

        int differenceX = position.y - initialPosition.y;

        if (moveRange < 0) {
            if (differenceX < moveRange) {
                vDir = VerticalDirection.DOWN;
            } else if (differenceX > 0) {
                vDir = VerticalDirection.UP;
            }
        } else {
            if (differenceX > moveRange) {
                vDir = VerticalDirection.UP;
            } else if (differenceX <= 0) {
                vDir = VerticalDirection.DOWN;
            }
        }
        //jesli przekracza zakres ruchu zmieniamy kierunek
        super.move(move);
    }

    @Override
    public void paint(Graphics2D g2d) {
        g2d.drawImage(image, position.x, position.y, null);
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    public void hit() {
        hp--;
        if (hp == 0) {
            toBeRemoved = true;
        }else if (hp==2){
            try {
                image = ImageIO.read(new File("src/main/resources/ham.png"));
            } catch (IOException ex) {
                System.err.println("nie moge zaladowac obrazka monstra");
            }
        }else if (hp==1){
            try {
                image = ImageIO.read(new File("src/main/resources/bacon.png"));
            } catch (IOException ex) {
                System.err.println("nie moge zaladowac obrazka monstra");
            }
        }

    }
}
