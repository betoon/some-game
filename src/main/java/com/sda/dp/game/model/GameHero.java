package com.sda.dp.game.model;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.dispatcher.HeroFireEvent;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GameHero extends AbstractGameObject {

    private BufferedImage image;
    public int healthPoints=3;



    public GameHero(int x, int y) {
        super(new Point(x, y));
        try {
            image = ImageIO.read(new File("src/main/resources/goku_small.png"));
        } catch (IOException ex) {
            System.err.println("nie moge zaladowac obrazka bohatera");
        }
    }

    public void fire(){
        int positionX=image.getHeight()/2+position.x;

        Dispatcher.instance.dispatch(new HeroFireEvent(positionX,position.y+50));
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(image, position.x, position.y, null);
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    @Override
    public int getHeight() {
        return image.getHeight();
    }

    @Override
    public int getWidth() {
        return image.getWidth();
    }

    @Override
    public void hit() {
        healthPoints--;
    }
}
