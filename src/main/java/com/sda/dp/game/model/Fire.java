package com.sda.dp.game.model;

import com.sda.dp.game.HorizontalDirection;

import java.awt.*;

public class Fire extends AbstractGameObject {

    private Color fireColor;

    public Fire(int x, int y, double speed, boolean right) {
        super(new Point(x, y));
        this.speed = speed;
        if (right) {
            hDir = HorizontalDirection.RIGHT;
            fireColor=Color.RED;
        } else {
            hDir = HorizontalDirection.LEFT;
            fireColor=Color.ORANGE;
        }
    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        Color lastColor = g2d.getColor();

        g2d.setColor(fireColor);
        g2d.fillRect(position.x, position.y, 30, 10);

        g2d.setColor(lastColor);
    }

    @Override
    public int getHeight() {
        return 10;
    }

    @Override
    public int getWidth() {
        return 30;
    }

    @Override
    public void hit() {

    }
}
