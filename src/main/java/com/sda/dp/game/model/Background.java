package com.sda.dp.game.model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Background extends AbstractGameObject {

    private BufferedImage backgroundImage;

    public Background() {
        try {
            backgroundImage= ImageIO.read(new File("src/main/resources/background_small.png"));
        } catch (IOException e) {
            System.err.println("nie moge znalezc obrazka tla!");
        }
    }

    @Override
    public int getHeight() {
        return backgroundImage.getHeight();
    }

    @Override
    public int getWidth() {
        return backgroundImage.getWidth();
    }

    @Override
    public void hit() {

    }

    @Override
    public void paint(Graphics2D g2d) {
        super.paint(g2d);
        g2d.drawImage(backgroundImage,0,0,null);

    }
}
