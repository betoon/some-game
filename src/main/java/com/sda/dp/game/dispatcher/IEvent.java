package com.sda.dp.game.dispatcher;

public interface IEvent {
    void run();
}
