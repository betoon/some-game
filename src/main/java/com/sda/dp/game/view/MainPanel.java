package com.sda.dp.game.view;

import com.sda.dp.game.dispatcher.Dispatcher;
import com.sda.dp.game.interfaces.IFireListener;
import com.sda.dp.game.model.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.Random;

public class MainPanel extends JPanel implements IFireListener {
    private final Color BACKGROUND_COLOR = Color.DARK_GRAY;
    private final int MAX_MONSTER_SHOTS = 3;
    private final int width;
    private final int height;

    private List<AbstractGameObject> objectList;
    private List<AbstractGameObject> fireList;
    private List<AbstractGameObject> monsterFireList;

    private GameHero hero;
    private GameText gameOverText;
    private GameText gameYouWonText;
    private Heart heart;
    private Background background;

    public MainPanel(int width, int height) {
        super();
        this.width = width;
        this.height = height;

        // create stuff
        objectList = new ArrayList<>();
        fireList = new ArrayList<>();
        monsterFireList = new ArrayList<>();
        heart = new Heart();

        background = new Background();

        gameYouWonText = new GameText(width / 2, height / 2, "Bacon is reaaaaady!");
        gameOverText = new GameText(width / 2, height / 2, "Fatality!");

        // set stuff
        setBorder(BorderFactory.createLineBorder(Color.black, 2));
        setLayout(null);

        Dispatcher.instance.registerObject(this);
    }

    public void move(double move) {
        Random r = new Random();


        if (move > 0) {
            //alokacja tablicy o rozmiarze oryginalnej listy
            AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
            //kopiowanie obiektow
            objectsToPaint = objectList.toArray(objectsToPaint);
            //iterujemy tablice zamiast listy
            //nie usuwamy z kolekcji ktora iterujemy
            //iterujemy tablice, usuwamy z listy
            for (AbstractGameObject objectToPaint : objectsToPaint) {
                if (objectToPaint != null) {
                    objectToPaint.move(move);
                }
                if (monsterFireList.size() < MAX_MONSTER_SHOTS) {
                    if (r.nextInt(1000) >= (1000 - objectList.size())) {
                        addMonsterFire(objectToPaint.getPositionX(), objectToPaint.getPositionY());
                    }
                }
            }

            AbstractGameObject[] firesToMove = new AbstractGameObject[fireList.size()];
            firesToMove = fireList.toArray(firesToMove);
            for (AbstractGameObject fire : firesToMove) {
                if (fire != null) {
                    fire.move(move);
                }
            }

            AbstractGameObject[] monsterFiresToMove = new AbstractGameObject[monsterFireList.size()];
            monsterFiresToMove = monsterFireList.toArray(monsterFiresToMove);
            for (AbstractGameObject monsterFire : monsterFiresToMove) {
                if (monsterFire != null) {
                    monsterFire.move(move);
                }
                if (monsterFire.getPositionX() < 0) {
                    monsterFireList.remove(monsterFire);
                }
            }
            hero.move(move);
        }
    }

    public void checkCollisions() {
        AbstractGameObject[] objectsToMove = new AbstractGameObject[objectList.size()];
        objectsToMove = objectList.toArray(objectsToMove);

        AbstractGameObject[] firesToMove = new AbstractGameObject[fireList.size()];
        firesToMove = fireList.toArray(firesToMove);

        // ConcurrentModificationException
        for (AbstractGameObject strzal : firesToMove) {
            if (!strzal.isToBeRemoved()) {
                for (AbstractGameObject postac : objectsToMove) {
                    if (postac.checkCollision(strzal) && !strzal.isToBeRemoved()) {
                        strzal.setToBeRemoved(true);
                        postac.hit();

                        if (postac.isToBeRemoved())
                            objectList.remove(postac);
                        fireList.remove(strzal);
                        // nie możemy usuwać
                        // usuwanie z iterowanej listy nie jest dozwolone
                    }
                }
            }
        }
        AbstractGameObject[] monsterShots = new AbstractGameObject[monsterFireList.size()];
        monsterShots = monsterFireList.toArray(monsterShots);

        for (AbstractGameObject monsterShot : monsterShots) {
            if (hero.checkCollision(monsterShot)) {
                hero.hit();
                monsterFireList.remove(monsterShot);
            }


        }
    }

    public void addCharacterIntoGame(AbstractGameObject gameObject) {
        objectList.add(gameObject);
    }

    public void addFireIntoGame(AbstractGameObject gameObject) {
        fireList.add(gameObject);
    }

    public void setGameHero(GameHero hero) {
        this.hero = hero;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(BACKGROUND_COLOR);
        g2d.fillRect(0, 0, width, height);

        if (hero.getHealthPoints() > 0 && objectList.size() != 0) {
            paintWorld(g2d);

        } else if (hero.getHealthPoints() == 0) {
            painGameOver(g2d);
        } else if (objectList.size() == 0) {
            painYouWon(g2d);
        }
    }

    private void painYouWon(Graphics2D g2d) {
        gameYouWonText.paint(g2d);
    }

    private void painGameOver(Graphics2D g2d) {
        gameOverText.paint(g2d);

    }

    private void paintWorld(Graphics2D g2d) {
        //****** tlo
        background.paint(g2d);
        //rysowanie zycia
        for (int i = 0; i < hero.getHealthPoints(); i++) {
            heart.paint(g2d, i);
        }


        g2d.setColor(Color.white);

        //rysowanie postaci
        AbstractGameObject[] objectsToPaint = new AbstractGameObject[objectList.size()];
        objectsToPaint = objectList.toArray(objectsToPaint);

        for (AbstractGameObject objectToPaint : objectsToPaint) {
            objectToPaint.paint(g2d);
        }
        //**********rysowanie strzlow potworow
        AbstractGameObject[] monsterFires = new AbstractGameObject[monsterFireList.size()];
        monsterFires = monsterFireList.toArray(monsterFires);


        for (AbstractGameObject monsterFire : monsterFires) {
            monsterFire.paint(g2d);
        }

        //***********rysowanie strzalow
        AbstractGameObject[] firesToPaint = new AbstractGameObject[fireList.size()];
        firesToPaint = fireList.toArray(firesToPaint);

        for (AbstractGameObject fireToPaint : firesToPaint) {
            fireToPaint.paint(g2d);
        }
        //*************
        hero.paint(g2d);
    }

    @Override
    public void addFire(int posX, int posY, double speed, boolean right) {
        addFireIntoGame(new Fire(posX, posY, speed, right));
    }

    public void addMonsterFire(int posX, int posY) {
        addMonsterFireIntoGame(new Fire(posX, posY, 6, false));
    }

    public void addMonsterFireIntoGame(Fire fire) {
        monsterFireList.add(fire);
    }
}
