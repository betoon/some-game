package com.sda.dp.game.interfaces;

public interface IFireListener {
    void addFire(int posX,int posY, double speed, boolean right);
}
